﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class Default1Controller : Controller
    {
        //
        // GET: /Default1/
        private static int counter = 0;

        private int getCookieCounter()
        {
            var cookie = Request.Cookies["counter"];

            if (cookie == null)
            {
                cookie = new HttpCookie("counter")
                {
                    Name = "counter",
                    Value = "0",
                    Expires = DateTime.Now.AddDays(2),

                };
                Response.SetCookie(cookie);
            }

            return Convert.ToInt32(cookie["Value"]);
        }

        private int getSessionCounter()
        {
            var sessionCounter = Session["sessionCounter"];
            if (sessionCounter == null)
            {
                Session["sessionCounter"] = "0";
                sessionCounter = 0;
            }
            return Convert.ToInt32(sessionCounter);
        }

        public ActionResult Index()
        {
            return View();
        }

        public String Date()
        {
            return DateTime.Now.ToLongTimeString();
        }


        public ActionResult ClickButton()
        {
            
            int cookieCounter = getCookieCounter();
            cookieCounter++;
            Response.Cookies["counter"].Values.Add("Value", cookieCounter.ToString());

            int sessionCounter = getSessionCounter();
            sessionCounter++;
            Session["sessionCounter"] = sessionCounter;

            counter++;
            return RedirectToAction("Counter");
        }

        public ActionResult Counter()
        {
            ViewBag.Counter = counter;
            ViewBag.cookieCounter = getCookieCounter();
            ViewBag.sessionCounter = getSessionCounter();
            return PartialView();
        }
	}
}