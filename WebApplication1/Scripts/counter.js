﻿$(document).ready(function () {
    $('#counter').load("Default1/Counter");
    $('#doge').hide();
    $('#ButtonC').mousedown(function (event) {
        event.preventDefault();
        $("#doge").show();
    });
    $('#ButtonC').click(function (event) {
        $('#doge').hide();
        var url = $(this).attr('href');
        $('#counter').load(url);
        event.preventDefault();
    });
});
